<?php
//
///**
// * Implementation of hook_field_info().
// */
//function cck_inputs_form_field_info() {
//  return array(
//    'cck_inputs_form' => array(
//      'label' => t('Input Form'),
//      'description' => t('Adds a form and shows a submit button to the user'),
//    ),
//  );
//}
//
///**
// * Implementation of hook_theme().
// */
//function cck_inputs_form_theme() {
//  return array(
//    'cck_inputs_form_formatter_default' => array(
//      'arguments' => array('element' => NULL),
//    ),
//
//    'form_widget' => array(
//      'arguments' => array('element' => NULL),
//    ),
//  );
//}
//
//
//
///**
// * Implementation of hook_field_settings().
// */
//function cck_inputs_form_field_settings($op, $field) {
//  switch ($op) {
//    case 'form':
//      $form = array();
//
//      return $form;
//
//    case 'save':
//      return array('form_action', 'form_method', 'form_enctype');
//
//    case 'database columns':
//      $columns['form_action'] = array('type' => 'varchar', 'length' => 255, 'not null' => false);
//      $columns['form_method'] = array('type' => 'varchar', 'length' => 255, 'not null' => false);
//      $columns['form_enctype'] = array('type' => 'varchar', 'length' => 255, 'not null' => false);
//
//      return $columns;
//  }
//}
//
///**
// * Implementation of hook_field().
// */
//function cck_inputs_form_field($op, &$node, $field, &$items, $teaser, $page) {
//  switch ($op) {
//    case 'validate':
//      if (is_array($items)) {
//        foreach ($items as $delta => $item) {
//          $error_element = isset($item['_error_element']) ? $item['_error_element'] : '';
//          if (is_array($item) && isset($item['_error_element'])) unset($item['_error_element']);
//        }
//      }
//      return $items;
//
//    case 'sanitize':
//      foreach ($items as $delta => $item) {
//        $items[$delta]['form_action'] = check_plain($item['form_action']);
//        $items[$delta]['form_method'] = check_plain($item['form_method']);
//        $items[$delta]['form_enctype'] = check_plain($item['form_enctype']);
//      }
//  }
//}
//
///**
// * Implementation of hook_content_is_empty().
// */
//function cck_inputs_form_content_is_empty($item, $field) {
//  if (empty($item['form_action']) && (string)$item['form_action'] !== '0') {
//    return TRUE;
//  }
//  return FALSE;
//}