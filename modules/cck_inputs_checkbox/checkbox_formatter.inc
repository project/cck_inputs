<?php



/**
 * Implementation of hook_field_formatter_info().
 */
function cck_inputs_checkbox_field_formatter_info() {
  return array(
    'default' => array(
      'label' => t('Default'),
      'field types' => array('cck_inputs_checkbox'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/**
 * Theme function for 'default' text field formatter.
 */
function theme_cck_inputs_checkbox_formatter_default($element) {
#print_r($element);exit;
  $checkbox = array(
    '#type' => 'checkbox',
    '#name' => $element['#item']['checkbox_name'],
    '#return_value' => $element['#item']['checkbox_value'],
    '#title' => t($element['#item']['checkbox_label']),
    '#default_value' => 0
  );
  return drupal_render($checkbox);

}

